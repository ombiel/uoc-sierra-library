var React = require("react");

var numbers = "zero,one,two,three,four,five,six,seven,eight,nine,ten,eleven,twelve,thirteen,fourteen,fifteen,sixteen".split(",");
var getChildren = require("-aek/react/utils/children");
var mergeProps = require("-aek/react/utils/merge-props");
var cloneElement = require("-aek/react/utils/clone");

var EventEmitter = require("-aek/event-emitter");

class RouterListener extends EventEmitter {
  constructor(router) {
    super();
    router.addRoute("*",(ctx,next)=>{
      this.emit("routechange",ctx);
      next();
    });
  }
}

var NavMenuItem = React.createClass({
  getDefaultProps:function() {
    return {
      component:"a",
      href:"#"
    };
  },

  render:function() {
    var props = {
      classSet:{
        item:1,
        active:this.props.active
      }
    };

    props = mergeProps(props,this.props);

    props.children = getChildren(this);

    if(props.icon) {
      props.children = [<i className={`${props.icon} icon`}></i>].concat(props.children);
    }

    if(props.badge != null && props.badge !== false) {
      var badgeProps = {
        className:"ui floating circular label",
        style:{
          //backgroundColor:"rgba(255,255,255,0.35)",
          backgroundColor:"#eb212e",
          left:"auto",
          right:"5%",
          top:"10%",
          margin:0
        }
      };

      badgeProps = mergeProps(badgeProps,props.badgeProps);

      props.children.push(<span {...badgeProps}>{props.badge}</span>);

    }

    return React.createElement(props.component,props);
  }
});

var NavMenu = React.createClass({

  getDefaultProps:function() {
    return {
      items:[],
      component:"nav",
      fluid:true,
      inverted:true,
      icon:true
    };
  },

  getInitialState:function() {
    return {};
  },

  componentDidMount:function() {
    this.checkRouter();
  },

  componentWillUnmount:function() {
    this.detachRouter();
  },

  componentDidUpdate:function() {
    this.checkRouter();
  },

  detachRouter:function() {
    if(this.router) {
      this.router._menuEmitter.off(this.routeChange);
    }
  },

  checkRouter:function() {
    if(this.props.router) {
      if(!this.router) {
        this.router = this.props.router;
        if(!this.router._menuEmitter) {
          this.router._menuEmitter = new RouterListener(this.router);
        }
        this.router._menuEmitter.on("routechange",this.routeChange);
        this.setState({currentPath:this.router.getCurrentPath()});
      }
    }
    else {
      this.detachRouter();
    }
  },

  routeChange:function(ctx) {
    this.setState({currentPath:ctx.path});
  },

  render:function() {

    var props = {
      classSet:{
        ui:1,
        fluid:this.props.fluid,
        inverted:this.props.inverted,
        labeled:this.props.icon,
        icon:this.props.icon
      },
      style:{
        borderRadius:0,
        marginTop:0
      }
    };

    if(this.props.top) {
      props.style.top = this.props.top + "px";
      props.style.position = "absolute";
    }

    props = mergeProps(props,this.props);

    if(props.theme) {
      props.className += ` ${props.theme}`;
    }

    var children = getChildren(this).map((child)=>{
      var childPath = child.props.href && child.props.href.replace("#","");
      if(childPath === this.state.currentPath) {
        return cloneElement(child,{active:true});
      }
      return child;
    });

    props.className += ` ${numbers[children.length]} item menu`;

    return React.createElement(props.component,props,children);

  }
});

module.exports = {NavMenu,NavMenuItem,Menu:NavMenu,MenuItem:NavMenuItem};
