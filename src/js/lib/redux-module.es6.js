var _ = require("-aek/utils");

function snakeCase(str) {
  return str.replace(/[A-Z]/g,function(match) {
    return "_" + match;
  }).toUpperCase();
}

class ReduxModule {

  constructor(name,initialState = {}) {
    this.name = name;
    this.initialState = initialState;
    this.actionHandlers = {};
    this.reducer = this.reducer.bind(this);
  }

  reducer(state,action) {
    state = _.merge({},this.initialState,state);
    var actionName = action.type;
    var actionHandler = this.actionHandlers[actionName];
    if(actionHandler) {
      return actionHandler(state,action);
    }
    else {
      return state;
    }
  }

  createDispatcher(name,fn) {
    var actionName = `${this.name}:${snakeCase(name)}`;
    var dispatcher = (payload)=>{
      this.store.dispatch({
        type:actionName,
        payload
      });
    };
    this[name] = dispatcher.bind(this);
    this.actionHandlers[actionName] = fn;

    return dispatcher;

  }

}

ReduxModule.getReducers = function(modules) {
  var reducers = {};
  _.forEach(modules,function(module,key) {
    reducers[key] = module.reducer;
  });
  return reducers;
};

ReduxModule.register = function(modules,store) {
  _.forEach(modules,function(module) {
    module.store = store;
  });
};

module.exports = ReduxModule;
