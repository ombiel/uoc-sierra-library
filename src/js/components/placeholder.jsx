var React = require("react");
var mergeProps = require("-aek/react/utils/merge-props");

var style = {
  height:"200px",  
  fontWeight:200,
  textAlign:"center",
  verticalAign:"middle",
  display:"table-cell",
  letterSpacing:"2px"
};

var Placeholder = React.createClass({

  render:function() {

    var props = mergeProps({},{style},this.props);

    return  (
      <div style={{display:"table",width:"100%"}}>
        <div {...props}/>
      </div>
    );
  }
});

module.exports = Placeholder;
