var aekFluxStore = require("-aek/flux-store");
var ReduxModule = require("../lib/redux-module");
var windowDimensions = require("-aek/reducers/window-dimensions")();

var modules = {
  userData:require("./redux-modules/user-data"),
  search:require("./redux-modules/search")
};

var reducers = ReduxModule.getReducers(modules);

reducers.$$$windowDimensions = windowDimensions;

var localStorageKey = "mdx-library/main-store";

var store = aekFluxStore({reducers,localStorageKey});

windowDimensions.setStore(store);

ReduxModule.register(modules,store);

module.exports = store;
