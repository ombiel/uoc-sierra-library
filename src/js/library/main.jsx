var React = require("-aek/react");
var Container = require("-components/container");
var {VBox} = require("-components/layout");
var {RouterView} = require("-components/router");
var router = require("./router");

var LinksPage = require("./pages/links");
var SearchPage = require("./pages/search-page");
var CombinedPage = require("./pages/combined-page");

var store = require("./store");
var userData = require("./redux-modules/user-data");
var {Menu,MenuItem} = require("../lib/nav-menu");

// Android scroll bug fix
require("-aek/patches/fix-android-pull-to-refresh");

var Screen = React.createClass({

  componentDidMount:function() {
    store.subscribe(()=>{
      this.forceUpdate();
    });
    userData.refreshData();
  },

  render:function() {

    var state = store.getState();
    var userData = state.userData && state.userData.data || {};

    var numOfLoans = userData.loanItems && userData.loanItems.length || 0;
    var numOfHolds = userData.holdItems && userData.holdItems.length || 0;
    var numOfFines = userData.fineItems && userData.fineItems.length || 0;
    var combinedBadge = (numOfLoans + numOfHolds + numOfFines) || null;

    return (
      <Container>
        <VBox>
          <RouterView router={router}>
            <CombinedPage path="/combined"/>
            <SearchPage path="/search"/>
            <LinksPage path="/links"/>
          </RouterView>
          <Menu router={router} flex={0} key="nav">
            <MenuItem icon="user" href="#/combined" badge={combinedBadge} badgeProps={"style={{background: #ff9e1b }}"}>My Account</MenuItem>
            <MenuItem icon = "search" href = "http://canterbury.summon.serialssolutions.com/#!/">Search</MenuItem>
            <MenuItem icon="info circle" href="#/links">Resources</MenuItem>
          </Menu>
        </VBox>

      </Container>
    );

  }

});

React.render(<Screen/>,document.body);
