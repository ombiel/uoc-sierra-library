var ReduxModule = require("../../lib/redux-module");
var request = require("-aek/request");
var _ = require("-aek/utils");

var userData = new ReduxModule("USER_DATA",{
  $$$status:{},
  $$$cancellations:{},
  $$$renewals:{},
  $$$reservations:{}
});

userData.createDispatcher("loadingData",(state)=>{
  return _.extend({},state,{$$$status:{loading:true,error:null}});
});

userData.createDispatcher("dataError",(state,action)=>{
  return _.extend({},state,{$$$status:{loading:false,error:action.payload}});
});

userData.createDispatcher("setData",(state,action)=>{
  return _.extend({},state,{$$$status:{loading:false,error:null},data:action.payload});
});


// Cancel Reservation
userData.createDispatcher("cancelling",(state,{payload})=>{
  var $$$cancellations = _.extend({},state.$$$cancellations,{[payload.holdKey]:{cancelling:true}});
  return _.extend({},state,{$$$cancellations});
});

userData.createDispatcher("cancelError",(state,{payload})=>{
  var $$$cancellations = _.extend({},state.$$$cancellations,{[payload.holdKey]:{error:payload.error}});
  return _.extend({},state,{$$$cancellations});
});

userData.createDispatcher("cancelSuccess",(state,{payload})=>{
  var {holdKey} = payload;
  var $$$cancellations = _.extend({},state.$$$cancellations,{[holdKey]:{success:true}});
  var reserved = _.reject(state.data && state.data.holdItems || [],{holdKey});
  var data = _.extend({},state.data,{reserved});
  return _.extend({},state,{$$$cancellations,data});
});


// Renew Loan
userData.createDispatcher("renewing",(state,{payload})=>{
  var $$$renewals = _.extend({},state.$$$renewals,{[payload.itemBarcode]:{renewing:true}});
  return _.extend({},state,{$$$renewals});
});

userData.createDispatcher("renewError",(state,{payload})=>{
  var $$$renewals = _.extend({},state.$$$renewals,{[payload.itemBarcode]:{error:payload.error}});
  return _.extend({},state,{$$$renewals});
});

userData.createDispatcher("renewSuccess",(state,{payload})=>{
  var $$$renewals = _.extend({},state.$$$renewals,{[payload.itemBarcode]:{success:true,successMessage:payload.message[0]}});
  return _.extend({},state,{$$$renewals});
});


// Reservation
userData.createDispatcher("reserving",(state,{payload})=>{
  var $$$reservations = _.extend({},state.$$$reservations,{[payload.titleKey]:{reserving:true}});
  return _.extend({},state,{$$$reservations});
});

userData.createDispatcher("reserveError",(state,{payload})=>{
  var $$$reservations = _.extend({},state.$$$reservations,{[payload.titleKey]:{error:payload.error}});
  return _.extend({},state,{$$$reservations});
});

userData.createDispatcher("reserveSuccess",(state,{payload})=>{
  var {titleKey} = payload;
  var $$$reservations = _.extend({},state.$$$reservations,{[titleKey]:{success:true}});
  return _.extend({},state,{$$$reservations});
});


function normaliseData(data) {
  data.loanItems = _.asArray(data.loanItems);
  data.holdItems = _.asArray(data.holdItems);
  data.fineItems = _.asArray(data.fineItems);

  // timestamp it
  data.lastUpdated = Date.now();

  return data;
}

userData.refreshData = function() {
  this.loadingData();
  request.action("get_data").end((err,res)=>{

    var data = res && res.body;
    if(err || !data) {
      err = err || Error("No Data");
      this.dataError(err);
    }
    else {
      this.setData(normaliseData(data));
    }
  });
};

userData.cancelReservation = function(holdKey) {
  this.cancelling({holdKey});
  request
  .action("cancel_reservation")
  .send({holdKey})
  .end((err,res)=>{
    var data = res && res.body;
    var success = data && data.success;
    if(!success) {
      var error = err || (data && data.error) || "Cancellation failed";
      this.cancelError({error,holdKey});
    }
    else {
      this.cancelSuccess({holdKey});
      if(data.new_data) {
        this.setData(normaliseData(data.new_data));
      }
    }
  });
};

userData.renewLoan = function(itemBarcode) {
  this.renewing({itemBarcode});
  request
  .action("renew_loan")
  .send({itemBarcode})
  .end((err,res)=>{
    var data = res && res.body;
    var success = data && data.success;
    if(!success) {
      var error = err || (data && data.error) || "Renewal failed: You have exceeded your renew limit of 10";
      this.renewError({error,itemBarcode});
    }
    else {
      var message = res.message || (data && data.message) || "Renewal successful";
      this.renewSuccess({message,itemBarcode});
      if(data.new_data) {
        this.setData(normaliseData(data.new_data));
      }
    }
  });
};

userData.reserve = function(titleKey) {
  this.reserving({titleKey,status:{reserving:true}});
  console.log(titleKey);
  request
  .action("reserve_item")
  .send({titleKey})
  .end((err,res)=>{
    var data = res && res.body;
    var success = data && data.success;
    if(!success) {
      var error = err || (data && data.error) || "Reservation failed";
      this.reserveError({error,titleKey});
    }
    else {
      this.reserveSuccess({titleKey});
      if(data.new_data) {
        this.setData(normaliseData(data.new_data));
      }
    }
  });
};

module.exports = userData;
