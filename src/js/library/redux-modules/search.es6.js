var ReduxModule = require("../../lib/redux-module");

var _ = require("-aek/utils");
var request = require("-aek/request");

let minYear = 1850;
let maxYear = (new Date()).getFullYear();

var search = new ReduxModule("SEARCH",{
  $$$searches:{},
  $$$searchTerm:"",
  items:{}
});

search.minYear = minYear;
search.maxYear = maxYear;

var tempKeys = ["searchTerm"];

// Function to package an object in a suitable form for state by prefixing relevant properties with $$$
search.pack = function(obj) {
  let packed = {};
  _.forEach(obj,(val,key)=>{
    let packedKey = _.includes(tempKeys,key) ? `$$$${key}` : key;
    packed[packedKey] = val;
  });
  return packed;
};

search.unpack = function(obj) {
  let unpacked = {};
  _.forEach(obj,(val,packedKey)=>{
    let key = (packedKey.indexOf("$$$") === 0) ? packedKey.substr(3) : packedKey;
    unpacked[key] = val;
  });
  return unpacked;
};

// Generate a key for storing searches
let genSearchKey = function({searchTerm}) {
  return searchTerm;
};


search.createDispatcher("update",(state,action)=>{
  return _.fuse({},state,search.pack(action.payload));
});


search.createDispatcher("setSearchResults",(state,action)=>{

  var items = {};
  var payload = action.payload;

  let searchKey = genSearchKey(payload);

  var storedSearch = state.$$$searches[searchKey];
  var existingResults = storedSearch && storedSearch.results || [];

  return _.fuse({},state,{
    items,
    $$$searches:{
      [searchKey]:{
        error:action.payload.error,
        loading:false,
        results:payload.results,
        total:payload.total
      }
    }
  });
});

search.createDispatcher("searchLoading",(state,action)=>{

  let searchKey = genSearchKey(action.payload);
  let storedSearch = state.$$$searches[searchKey];
  let results = storedSearch && storedSearch.results;

  return _.fuse({},state,{
    $$$searches:{
      [searchKey]:{error:null,loading:true,results:results}
    }
  });

});

// Fetch results based on current state
search.fetchResults = function() {

  var state = this.store.getState();
  var unpacked = search.unpack(state.search);
  var {searchTerm} = unpacked;
  var searchKey = genSearchKey(search.unpack(state.search));
  var storedSearch = state.search.$$$searches[searchKey];
  var existingResults = storedSearch && storedSearch.results || [];

  this.searchLoading(unpacked);
  request.action("search_books").send({query:searchTerm}).end((error,res)=>{

    var data = res.body.results;
    var dataItems = data.searchResultBookItem;
    var total = dataItems ? dataItems.length : false;
    var results;

    if(!_.isEmpty(dataItems)) {
      if(!total) {
        // Object
        results = [];
        results.push(data.searchResultBookItem);
        total = 1;
      }
      else {
        // Array
        results = data.searchResultBookItem;
        total = results.length ? results.length : 0;
      }
    }
    else {
      // When data is empty object or returns undefined
      results = false;
      error = "No results found";
    }

    if(!error && !results) {
      error = Error("No Data Returned");
    }

    this.setSearchResults({searchTerm,error,results,total});
  });
};

// update state with payload and if no results already stored, go fetch some
search.getResults = function(payload) {

  search.update(payload);

  var state = this.store.getState().search;

  // if the searchTerm is empty, do nothing
  if(!state.$$$searchTerm || state.$$$searchTerm === "") {
    search.update({searchTerm:""});
    return;
  }

  var searchKey = genSearchKey(this.unpack(state));

  var storedSearch = state.$$$searches[searchKey];

  if(!storedSearch || !storedSearch.results) {
    search.fetchResults();
  }

};

// Return the stored search based on current state
search.getResultsForCurrentState = function() {
  var state = this.store.getState().search;

  // Special case if searchTerm is empty
  if(!state.$$$searchTerm) {
    return {noSearchTerm:true};
  }

  var searchKey = genSearchKey(this.unpack(state));

  var searchResponse = state.$$$searches[searchKey] || {};

  let filteredResults = searchResponse.results;

  searchResponse = _.extend({},searchResponse,{filteredResults});

  return searchResponse;
};

search.getSearchParams = function() {
  return this.unpack(this.store.getState().search);
};

search.loadMore = function() {
  this.fetchResults();
};

module.exports = search;
