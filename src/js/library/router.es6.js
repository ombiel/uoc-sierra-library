var {AekReactRouter} = require("-components/router");

var router = new AekReactRouter();

// default to search page
router.addRoute("/",()=>{
  router.replace("/combined");
});

module.exports = router;
