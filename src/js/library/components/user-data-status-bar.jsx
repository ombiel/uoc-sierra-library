var React = require("react");
var {ErrorMessage,InfoMessage} = require("-components/message");
var Button = require("-components/button");
var moment = require("moment");

var store = require("../store");
var userData = require("../redux-modules/user-data");


var UserDataStatusBar = React.createClass({

  refreshClick:function(ev) {
    ev.preventDefault();
    userData.refreshData();
  },

  componentDidMount:function() {
    // rerender every 30s to update relative time
    this.refreshInterval = setInterval(()=>{
      this.forceUpdate();
    },30000);
  },

  componentWillUnmount:function() {
    clearInterval(this.refreshInterval);
  },

  render:function() {
    var state = store.getState();
    var userData = state.userData;
    var {error,loading} = userData.$$$status;
    var data = userData.data;
    var lastUpdated = data && data.lastUpdated;
    var messageComponent = error && ErrorMessage || (loading && InfoMessage || InfoMessage);
    var errorMessage;
    var buttonProps = {
      variation:"info",
      size:"mini",
      icon:"refresh",
      className:"right floated",
      style:{marginRight:"-1.4em",marginTop:"-0.4em"},
      onClick:!loading && this.refreshClick,
      loading
    };


    var messageText = "";

    if(loading) {
      if(!lastUpdated) {
        messageText = "Loading...";
      }
    }

    if(error) {
      errorMessage = <div>Sorry, unable to sync at this time</div>;
    }

    if(lastUpdated) {
      messageText += ` Last updated: ${moment(lastUpdated).fromNow()}`;
    }

    return React.createElement(messageComponent,{inlineIcon:true,icon:false},[<Button {...buttonProps}/>,errorMessage,<div>{messageText}</div>]);

  }
});

module.exports = UserDataStatusBar;
