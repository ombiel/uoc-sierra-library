var Page = require("-components/page");
var React = require("react");
var {VBox,Panel} = require("-components/layout");
var {BasicSegment,Segment} = require("-components/segment");
var {BannerHeader,Header} = require("-components/header");
var {Listview,Item} = require("-components/listview");
var {SuccessMessage} = require("-components/message");
var Button = require("-components/button");
var Placeholder = require("../../components/placeholder");
var moment = require("moment");

// Loading/Refresh Bar
var UserDataStatusBar = require("../components/user-data-status-bar");

// UserData
var userData = require("../redux-modules/user-data");
var store = require("../store");

var CombinedPage = React.createClass({

  getInitialState:function() {
    return {
      issuedOpen: true,
      reservedOpen: true,
      finedOpen: true
    };
  },

  issuedToggle: function(ev){
    ev.preventDefault();
    var open = this.state.issuedOpen;

    if(open){
      this.setState({issuedOpen:false});
    }else{
      this.setState({issuedOpen:true});
    }
  },

  reservedToggle: function(ev){
    ev.preventDefault();
    var open = this.state.reservedOpen;

    if(open){
      this.setState({reservedOpen:false});
    }else{
      this.setState({reservedOpen:true});
    }
  },

  finedToggle: function(ev){
    ev.preventDefault();
    var open = this.state.finedOpen;

    if(open){
      this.setState({finedOpen:false});
    }else{
      this.setState({finedOpen:true});
    }
  },

  cancelReservation: function(holdKey,ev) {
    if(ev) {
      ev.preventDefault();
    }
    userData.cancelReservation(holdKey);
  },

  renewLoan: function(itemBarcode,ev) {
    if(ev) {
      ev.preventDefault();
    }
    userData.renewLoan(itemBarcode);
  },

  render:function() {

    var state = store.getState();
    var screenIsSmall = state.$$$windowDimensions.width < 450;
    // Data
    var userData = state.userData;
    var status = userData.$$$status;
    var data = userData && userData.data || {};
    // Data -> Items
    var loanItems = data.loanItems || [];
    var reserveItems = data.holdItems || [];
    var fineItems = data.fineItems || [];
    // Loading
    var loanLoading = status.loading && !loanItems.length;
    var reserveLoading = status.loading && !reserveItems.length;
    var fineLoading = status.loading && !fineItems.length;

    var loanContent, loansTotal, issuedOpenIcon, issuedContent;
    var reserveContent, reserveTotal, reservedOpenIcon, reserveHolderContent;
    var fineContent, finesTotal, finedOpenIcon, finedContent;

    var issuedOpen = this.state.issuedOpen;
    var reservedOpen = this.state.reservedOpen;
    var finedOpen = this.state.finedOpen;

    if(!loanLoading) {
      loanContent = <Placeholder style={{height:"50px"}}>You have no borrowed items</Placeholder>;
    }
    if(!reserveLoading) {
      reserveContent = <Placeholder style={{height:"50px"}}>You have no requested items</Placeholder>;
    }
    if(!fineLoading) {
      fineContent = <Placeholder style={{height:"50px"}}>You have no charges</Placeholder>;
    }

    //  L O A N S

    if(loanItems.length) {
      loansTotal = loanItems.length;
      loanContent = <Listview formatted flush items={loanItems} stackLabels={screenIsSmall} itemFactory={(item) => {
        var title = item.title.replace(/:|\//g, "");
        var due;
        var itemProps = {};
        if(item.dueDate) {
          var dueDate = moment(item.dueDate);
          due = <small style={{color:"grey"}}><b>Due Date:</b> {dueDate.format("Do MMMM YYYY")}</small>;
          // itemProps.labelVariation = "positive";
          // if(dueDate.isBefore()) {
          //   itemProps.labelVariation = "negative";
          // } else if(dueDate.isBefore(moment().add(2,"d"))) {
          //   itemProps.labelVariation = "warning";
          // }
        }

        // var a = moment();
        // var b = moment(item.dueDate);
        // var daysOverdue = a.diff(b, 'days');
        // if(daysOverdue < 0) {
        //   daysOverdue = 0;
        // }

        // Dispatcher in user-data.es6.js
        var loanStatus = state.userData.$$$renewals[item.itemBarcode] || {};
        var renewing = loanStatus.renewing;

        var renewButton = "";
        if(loanStatus.error)
        {
          //var renewError = loanStatus.error && <ErrorMessage inlineIcon>Renew Failed</ErrorMessage>;
          // Replace due date with error message
          due = <small style={{color:"red"}}><i className="remove icon"></i> {loanStatus.error}</small>;
        }
        else if(loanStatus.success)
        {
          //renewButton = <SuccessMessage icon="check">Renewed</SuccessMessage>;
          // Replace due date with success message
          due = <small style={{color:"green"}}><i className="check icon"></i> {loanStatus.successMessage}</small>;
        }
        else {
          // Show button if no loanStatus
          renewButton = <Button loading={renewing} disabled={renewing} onClick={this.renewLoan.bind(this,item.itemBarcode)} className="action">Renew</Button>;
        }

        return (
          <Item {...itemProps}>
            <div>
              <div style={{float:"right",marginLeft:"0.7em"}}>
                {renewButton}
              </div>
              <h4>{title}</h4>
              {item.author ? <h5 style={{fontWeight:"normal"}}>{item.author}</h5> : ""}
              {due}
            </div>
          </Item>
        );
      }}/>;
    }
    else {
      loansTotal = 0;
    }

    //  H O L D S

    if(reserveItems.length) {
      reserveContent = <Listview formatted flush items={reserveItems} stackLabels={screenIsSmall} itemFactory={(item) => {
        var title = item.title.replace(/:|\//g, "");
        var reserveMoment = moment(item.requestDate);
        var expiryDate = moment(item.expireDate);

        // Dispatcher in user-data.es6.js
        var reservationStatus = state.userData.$$$cancellations[item.holdKey] || {};
        var cancelling = reservationStatus.cancelling;
        //var cancelError = reservationStatus.error && <ErrorMessage compact inlineIcon>Cancellation Failed: {reservationStatus.error}</ErrorMessage>;

        var cancelButton, cancelError;
        if(reservationStatus.success) {
          cancelButton = <SuccessMessage compact inlineIcon>Cancelled</SuccessMessage>;
        }
        else if(reservationStatus.error) {
          cancelError = <small style={{color:"#912D2B"}}><i className="remove icon"></i> {reservationStatus.error}</small>;
        }
        else {
          cancelButton = <Button loading={cancelling} disabled={cancelling} onClick={this.cancelReservation.bind(this,item.holdKey)} className="action">Cancel</Button>;
        }

        return (
          <Item>
            <div>
              <div style={{float:"right",marginLeft:"0.7em"}}>
                {cancelButton}
              </div>
              <h4>{title}</h4>
              <small><b>Request Date:</b> {reserveMoment.format("Do MMMM YYYY")}<br />
              <b>Pickup Location:</b> {item.pickupLocDescription}<br />
              <b>Expiry Date:</b> {expiryDate.format("Do MMMM YYYY")}</small>
              {cancelError}
            </div>
          </Item>
        );
      }}/>;
      reserveTotal = reserveItems.length;
    }
    else {
      reserveTotal = 0;
    }

    //  F E E S

    if(fineItems.length) {
      finesTotal = fineItems.length;
      fineContent = <Listview formatted flush items={fineItems} stackLabels={screenIsSmall} itemFactory={(item) => {
        var description = item.description.replace(/:|\//g, "");
        var due,amount;
        var itemProps = {};
        if(item.dueDate) {
          var dueDate = moment(item.dueDate);
          due = <small style={{color:"grey"}}><b>Due Date:</b> {dueDate.format("Do MMMM YYYY")}</small>;
        }
        if(item.balance){
          amount = parseFloat(item.balance);
          amount = amount || 0;
          amount = amount.toFixed(2);
        }

        return (
          <Item {...itemProps}>
            <div>
              <h4 style={{float:"right",marginLeft:"0.7em"}}>
                ${amount}
              </h4>
              {item.reason ? <h4 >{item.reason}</h4> : ""}
              <small style={{color:"grey"}}><b>Title:</b> {description}</small><br />
              {due}
            </div>
          </Item>
        );
      }}/>;
    }
    else {
      finesTotal = 0;
    }


    // Container for Loans
    if(!issuedOpen){
      issuedOpenIcon = "angle down icon";
      issuedContent = null;
    }else{
      issuedOpenIcon = "angle up icon";
      issuedContent = !loanLoading ? <Segment>{ loanContent }</Segment> : "";
    }
    // Container for Holds
    if(!reservedOpen){
      reservedOpenIcon = "angle down icon";
      reserveHolderContent = null;
    }else{
      reservedOpenIcon = "angle up icon";
      reserveHolderContent = !reserveLoading ? <Segment>{ reserveContent }</Segment> : "";
    }
    // Container for Fines
    if(!finedOpen){
      finedOpenIcon = "angle down icon";
      finedContent = null;
    }else{
      finedOpenIcon = "angle up icon";
      finedContent = !fineLoading ? <Segment>{ fineContent }</Segment> : "";
    }


    return (
      <Page>
        <VBox>
          {/* { !loanLoading || !reserveLoading ?
            <BannerHeader level="4" flex={0} style={{padding:"0",margin:"0",fontSize:"1.5rem",fontWeight:"200"}} key="header" theme="prime" iconAlign="right">{data.name ? data.name : "-"}</BannerHeader>
            : null
          } */}
          <BannerHeader level="4" flex={0} style={{padding:"0",margin:"0",fontSize:"1.5rem",fontWeight:"200"}} key="header" theme="prime" iconAlign="right">My Account</BannerHeader>
          <Panel>
            <BasicSegment>
              <UserDataStatusBar />
              {!loanLoading && !reserveLoading && !fineLoading ?
                <a href="#" onClick={this.issuedToggle} style={{display:"block"}}>
                  <Segment inverted theme="alt">
                    <Header inverted level="3" theme="alt" style={{padding:"0"}}><span>Currently Borrowed ({ loansTotal })</span><div href="#" className="menuToggle" ><i className={issuedOpenIcon} /></div></Header>
                  </Segment>
                </a>
                : null }
              {!loanLoading && !reserveLoading && !fineLoading ?
                issuedContent
                : null}
              {!loanLoading && !reserveLoading && !fineLoading ?
                <a href="#" onClick={this.reservedToggle} style={{display:"block"}}>
                  <Segment inverted theme="alt">
                    <Header inverted level="3" theme="alt"><span>Requested ({ reserveTotal })</span><div href="#" className="menuToggle" ><i className={reservedOpenIcon} /></div></Header>
                  </Segment>
                </a>
                : null }
              {!loanLoading && !reserveLoading && !fineLoading ?
                reserveHolderContent
                : null}
              {!loanLoading && !reserveLoading && !fineLoading ?
                <a href="#" onClick={this.finedToggle} style={{display:"block"}}>
                  <Segment inverted theme="alt">
                    <Header inverted level="3" theme="alt" style={{padding:"0"}}><span>Charges ({ finesTotal })</span><div href="#" className="menuToggle" ><i className={finedOpenIcon} /></div></Header>
                  </Segment>
                </a>
                : null }
              {!loanLoading && !reserveLoading && !fineLoading ?
                finedContent
                : null}
            </BasicSegment>
          </Panel>
        </VBox>
      </Page>
    );
  }

});


module.exports = CombinedPage;
