var Page = require("-components/page");
var React = require("react");
var {VBox} = require("-components/layout");
var {BasicSegment} = require("-components/segment");
var _ = require("-aek/utils");

var LinksPage = React.createClass({

  render:function() {

    var link1,link2,link3,link4;

    var title1 = "Library Home";
    var title2 = "UC TopUp";
    var title3 = "Room Bookings";

    if(_.isWeb()) {
      // HTML href
      link1 = <li className="item"><a href="https://www.canterbury.ac.nz/library/" title={title1} target="_blank">{title1}</a></li>;
      link2 = <li className="item"><a href="https://topup.canterbury.ac.nz" title={title2} target="_blank">{title2}</a></li>;
      link3 = <li className="item"><a href="http://library.canterbury.ac.nz/webapps/mrbs/day.php" title={title3} target="_blank">{title3}</a></li>;
    }
    else {
      // Native links require campusm://openURL schema
      var url1 = "campusm://openURL?url=" + encodeURIComponent("https://www.canterbury.ac.nz/library/") + "&type=external";
      link1 = <li className="item"><a href={url1} target="_blank">{title1}</a></li>;
      var url2 = "campusm://openURL?url=" + encodeURIComponent("https://topup.canterbury.ac.nz") + "&type=external";
      link2 = <li className="item"><a href={url2} target="_blank">{title2}</a></li>;
      var url3 = "campusm://openURL?url=" + encodeURIComponent("http://library.canterbury.ac.nz/webapps/mrbs/day.php") + "&type=external";
      link3 = <li className="item"><a href={url3} target="_blank">{title3}</a></li>;
    }

    return (
      <Page>
        <VBox>
          <BasicSegment className="links">
            <ul className="ui listview menu links">
              {link1}
              {link2}
              {link3}
            </ul>
          </BasicSegment>
        </VBox>
      </Page>
    );

  }

});

module.exports = LinksPage;