var Page = require("-components/page");
var React = require("react");
var {VBox,Panel} = require("-components/layout");
var {BasicSegment} = require("-components/segment");
var Button = require("-components/button");
var Input = require("-components/input");
var store = require("../store");
var search = require("../redux-modules/search");

var _ = require("-aek/utils");
var {Button} = require("-components/button-group");

// This builds content brought back from the search or returns an error message
var CatalogueSearch = require("./search-page-components/catalogue-search");

var SearchPage = React.createClass({

  getInitialState:function() {
    var state = store.getState();
    return {
      searchTerm:state.search.$$$searchTerm
    };
  },

  componentDidMount:function() {
    // debouncing the global state change of the search value so it's only modified when user has stopped typing
    this.debouncedSetSearchTerm = _.debounce(this.setSearchTerm,1000);
    this.debouncedSetEmptySearchTerm = _.debounce(this.checkSearchTerm,1000);
    document.getElementById('searchBox').addEventListener('scroll', this.handleScroll);
  },

  componentWillUnmount: function() {
    document.getElementById('searchBox').removeEventListener('scroll', this.handleScroll);
  },

  handleScroll: function(event) {

    let scrollTop = event.srcElement.scrollTop;

    if(scrollTop > 300)
    {
      document.getElementById('backToTop').style.display = 'block';
      document.getElementById('searchResults').style.paddingBottom = '5em';
    }
    else
    {
      document.getElementById('backToTop').style.display = 'none';
      document.getElementById('searchResults').style.paddingBottom = '0';
    }
  },

  backToTop: function(ev){
    ev.preventDefault();
    document.getElementById('searchBox').scrollTop = 0;
    document.getElementById('backToTop').style.display = 'none';
  },

  setSearchTerm:function(term) {
    search.getResults({searchTerm:term});
  },

  checkSearchTerm:function(){
    if(!this.state.searchTerm || this.state.searchTerm === ""){
      search.update({searchTerm:""});
    }
  },

  searchTermChange:function(ev) {
    if(ev) {
      ev.preventDefault();
    }

    this.setState({searchTerm:ev.target.value});

    let state = store.getState();

    this.debouncedSetEmptySearchTerm();

    if(!_.isNative())
    {
      this.debouncedSetSearchTerm();
    }
  },

  onClear:function(ev) {
    ev.preventDefault();
    this.setState({searchTerm:""});
    search.getResults({searchTerm:""});
  },

  onSubmit:function(ev) {
    if(ev) {
      ev.preventDefault();
    }
    let state = store.getState();
    var term = this.state.searchTerm;
    this.setSearchTerm(term);
  },

  render:function() {

    var state = store.getState().search;

    var loading = false;
    var searchResults;
    var searchInput = this.state.searchTerm;
    var searchIcon;

    // Search icon if no input otherwise show Delete icon
    if(!searchInput){
      searchIcon = "search";
    }
    else {
      searchIcon = "delete";
    }

    return (
      <Panel>
        <VBox>
          <Page id="searchBox" onscroll={this.onscrollEvent}>
            <BasicSegment key="searchInput">
              <form onSubmit={this.onSubmit}>
                <Input icon={searchIcon} iconProps={{onClick:this.onClear}} fluid placeholder="Title, Author" onChange={this.searchTermChange} value={this.state.searchTerm} type="search">
                  <input style={{cursor:"pointer"}} id="search" name="search" />
                  <Button variation="alt">Search</Button>
                </Input>
              </form>
            </BasicSegment>
            <BasicSegment id="searchResults" style={{paddingTop:"0px"}} loading={loading && !searchResults}>
              <CatalogueSearch />
            </BasicSegment>
          </Page>
        </VBox>
        <a id="backToTop" onClick={this.backToTop}>⇡ Back to top</a>
      </Panel>
    );
  }

});


module.exports = SearchPage;
