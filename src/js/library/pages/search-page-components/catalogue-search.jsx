var React = require("react");
var {BasicSegment} = require("-components/segment");
var {ErrorMessage} = require("-components/message");
var search = require("../../redux-modules/search");

var SearchResults = require("./search-results");
//var SearchFilters = require("./search-filters");

var CatalogueSearch = React.createClass({

  filterToggle: function(ev){
    ev.preventDefault();
    search.toggleFilters();
  },

  render:function() {

    var content;

    var {loading,filteredResults,error,total,results} = search.getResultsForCurrentState();

    if (loading && !results) {
      content = null;
    }
    else if (error) {
      content = <ErrorMessage style={{textAlign:"center"}}>{error}</ErrorMessage>;
    }
    else if (results && results.length === 0) {
      //content = <div><SearchFilters showRefineButton={results && results.length}/><ErrorMessage style={{textAlign:"center"}}>No results found</ErrorMessage></div>;
      content = <div><ErrorMessage style={{textAlign:"center"}}>No results found</ErrorMessage></div>;
    }
    else if (results) {
      //content = <div><SearchFilters showRefineButton={results && results.length}/><SearchResults filtered={filteredResults} results={results} total={total} loading={loading} /></div>;
      content = <div><SearchResults filtered={filteredResults} results={results} total={total} loading={loading} /></div>;
    }

    return (
      <div>
        <BasicSegment style={{paddingTop:"0px"}} loading={loading && !results}>
          {content}
        </BasicSegment>
      </div>
    );
  }

});


module.exports = CatalogueSearch;
