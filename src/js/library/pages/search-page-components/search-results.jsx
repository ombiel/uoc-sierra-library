var React = require("react");
var {Listview,Item} = require("-components/listview");
var {SuccessMessage} = require("-components/message");
var Button = require("-components/button");
var _ = require("-aek/utils");

// UserData
var userData = require("../../redux-modules/user-data");
var store = require("../../store");

var SearchResults = React.createClass({

  makeReservation: function(titleKey,ev) {
    if(ev) {
      ev.preventDefault();
    }
    userData.reserve(titleKey);
  },

  render() {

    var state = store.getState();

    //var {filtered,results,total,loading} = this.props;
    var {results,total} = this.props;

    // Total
    var totalNumOfRecords;
    if(total > 0) {
      totalNumOfRecords = <p style={{color:"#999",borderBottom:"1px solid #EEE",lineHeight:"2em"}}>{total} results found</p>;
    }

    // Sort results by materialType
    var sorted = _.sortBy(results, 'materialType');

    return (
      <div>
        {totalNumOfRecords}
        <Listview formatted flush items={sorted} itemFactory={(item)=>{

          var icon = "help icon";
          switch(item.materialType) {
            case "BOOK":
              icon = "book icon";
              break;
            case "VIDEODISC":
              icon = "film icon";
              break;
            case "SOUNDREC":
              icon = "music icon";
              break;
            case "ER":
              icon = "browser icon";
              break;
            case "UNSPECIFIED":
              icon = "help icon";
              break;
          }

          var publisher = item.publisher || item.pubDate || "";
          var title = item.title || "";
          title = title.replace("/", "");
          var author = item.primaryAuthor || "";

          // Dispatcher in user-data.es6.js
          var reserveStatus = state.userData.$$$reservations[item.titleKey] || {};
          var reserving = reserveStatus.reserving;

          var reserveButton, reserveError;
          if(reserveStatus.success) {
            reserveButton = <small style={{color:"green"}}><i className="check icon"></i> Requested</small>;
          }
          else if(reserveStatus.error) {
            reserveError = <small style={{color:"red"}}><i className="remove icon"></i> {reserveStatus.error}</small>;
          }
          else {
            reserveButton = <Button loading={reserving} disabled={reserving} onClick={this.makeReservation.bind(this,item.titleKey)} className="action">Request</Button>;
          }

          return (
            <Item key={item.titleKey} icon={icon}>
              <h2 style={{fontSize:"1.2em"}}>{title}</h2>
              <p>{author}</p>
              <p className="small">{publisher}</p>
              {reserveButton}{reserveError}
            </Item>
          );

        }}/>
      </div>
    );
  }
});

module.exports = SearchResults;
