var React = require("react");
var {Listview,Item} = require("-components/listview");
var {BannerHeader} = require("-components/header");
var {BasicSegment} = require("-components/segment");


var dropMenu = (
  <BasicSegment style={{position:"absolute",right:0}}>
    <Listview formatted style={{marginTop:"0"}}>
        {/* <Item href="http://arts.ac.libanswers.com/mobile.php">Ask a question</Item> */}
        <Item href="http://arts.ac.libanswers.com/">Ask a question</Item>
        <Item href="http://bt2ha9xt3y.search.serialssolutions.com/">Find e-journals</Item>
        <Item href="http://arts.ac.libguides.com/az.php">Database A-Z list</Item>
        <Item href="https://myintranet.arts.ac.uk/staffandstudents/library-learning-and-teaching/library-services/passwords/">E-resources Passwords</Item>
    </Listview>
  </BasicSegment>
);

var SearchPageHeader = React.createClass({

  getInitialState() {
    return {open:false};
  },

  menuToggle(ev) {
    ev.preventDefault();
    this.setState({
      open:!this.state.open
    });
  },

  render() {

    let {open} = this.state;
    let openIcon = open ? "angle up icon" : "angle down icon";

    return (
      <div>
        <a href="#" onClick={this.menuToggle} style={{display:"block"}}>
          <BannerHeader level="4" style={{padding:"0",margin:"0",fontSize:"1.5rem",fontWeight:"200"}} key="header" theme="prime" iconAlign="right">
            Search
            <div className="menuToggle">
              <i className={openIcon} />
            </div>
          </BannerHeader>
        </a>
        <div style={{position:"relative",zIndex:100}}>
          { open ? dropMenu : null }
        </div>
      </div>
    );
  }

});

module.exports = SearchPageHeader;
