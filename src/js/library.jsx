require("./library/main");
var _ = require("-aek/utils");
// extend aek utils - this method should probably be added to the main utils module in aek-lib

_.asArray = function(obj,opts = {}) {
  var out = [];

  if(_.isArray(obj)) {
    out = obj;
  }
  else if(obj) {
    out = [obj];
  }

  if(!opts.includeEmptyObjects) {
    out = out.filter(function(item) {
      return !_.isEmpty(item);
    });
  }

  return out;
};
